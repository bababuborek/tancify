# Tancify

Tanulas segito webalkalmazas, ami a targyak elvegzeset segiti, a tananyag beosztasaval, flashcardokkal valo elsajatitasaval es allando szamonkeresekkel. Heti osszesiteseket kuld, amivel emlekezteti a regisztralt felhasznalot elorehaladasarol.

## Funkcio tervek

* "flashcards"

    * tanulas
    * teszt
    * hozzaadas / szerkesztes

* emlekezteto funkcio

    * valaszthato funkciok

* tananyag beosztasa

    * optimalis menetrend kiszamitasa
    * szemelyre szabas

* orarend